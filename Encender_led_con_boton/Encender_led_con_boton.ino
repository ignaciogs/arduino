/*
Programa para encender/apagar un led con un switch(botn)
*/

const int pinLed = 5; // Salida del led
const int pinBtn = 10; // Salida del botón

int estado = 0;
int anterior = 0;
int encender = 0;

void setup() {
  Serial.begin(9600);        //Prepara el monitor serial para mandar informacin como logs.
  pinMode (pinLed, OUTPUT);  //Configura la salida donde esta el Led como de salida
  pinMode (pinBtn, INPUT);   //Configura la salida donde esta el switch como de entrada
}

void loop() {
  estado = digitalRead(pinBtn);  //Leemos el estado del boton
  Serial.println(estado);        //Imprimimos en el monitor es estado del boton
  
  if (estado && anterior == 0) {
    encender = 1 - encender;
    delay(30);
  }
  anterior = estado;
  
  if (encender) {
    digitalWrite (pinLed, HIGH);
  } else {
    digitalWrite(pinLed, LOW);
  }
}
