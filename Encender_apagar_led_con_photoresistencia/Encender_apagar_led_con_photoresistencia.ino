/*
Control de un Led con una photoresistencia
*/

const int pinPhotoResistor = 0;  //Entrada analogica de la photoresistencia
const int pinLed = 12;           // Pin de salida del Led

void setup() {
  Serial.begin(9600);       //Preparacion del serial para escribir logs
  pinMode(pinLed, OUTPUT);  //
}

void loop() {
  int value = analogRead(pinPhotoResistor);  //Se consulta el valor de la photoresistencia
  Serial.println(value);                     //Se escribe un log con el valor de la fotoresistencia
  
  if ( value <= 900) {
    digitalWrite(pinLed, LOW);              //Apagamos el Led
  } else {
    digitalWrite(pinLed, HIGH);             //Encendemos el Led
  }
  
  delay(1000);
} 
