/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 9;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  for(int i = 0; i <= 255; i++) {
    analogWrite(led, i);
    delay(7);  
  }
  delay(1000);
  
  for(int i = 255; i >= 0; i--) {
    analogWrite(led, i);
    delay(7);  
  }
  delay(1000);
  /*analogWrite(led, 100);  // turn the LED on (HIGH is the voltage level)
  delay(2000);               // wait for a secondu
  analogWrite(led, 160);    // turn the LED off by making the voltage LOW
  delay(2000);               // wait for a second
  analogWrite(led, 255);
  delay(2000);
  */
}

